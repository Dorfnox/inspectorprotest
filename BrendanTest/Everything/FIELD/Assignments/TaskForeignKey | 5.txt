Basetable ID:	133
Field ID:	5
Name:	TaskForeignKey
Data Type:	1
Type:	1
Shadow Type:	
Comment:	Unique identifier of each record in the related table
Calculation Always Evaluate:	
Calculation Storage:	

Auto-Enter Value:	
Auto-Enter Allow Editing:	1
Auto-Enter Calculation:	0
Auto-Enter Lookup:	0
Auto-Enter Furigana:	0
Auto-Enter Always Evaluate:	
Auto-Enter Overwrite Existing Value:	
Auto-Enter Constant:	0
Auto-Enter Constant Data:	
Auto-Enter Generate:	
Auto-Enter Next Value:	
Auto-Enter Increment:	
Auto-Enter Lookup Table:	
Auto-Enter Lookup Table ID:	
Auto-Enter Lookup Field:	
Auto-Enter Lookup Field ID:	
Auto-Enter Lookup Field Table:	
Auto-Enter Lookup Field Repetition:	
Auto-Enter Lookup No Match Copy Option:	
Auto-Enter Lookup Copy Constant Value:	
Auto-Enter Lookup Copy Empty Value:	

Validation Type:	Only During Data Entry
Validation Always Validate Calculation:	0
Validation Calculation:	0
Validation Value List:	0
Validation Max:	0
Validation Message:	0
Validation Strict Data Type:	
Validation Not Empty:	0
Validation Unique:	0
Validation Existing:	0
Validation Value List Name:	
Validation Value List ID:	
Validation Range From:	
Validation Range To:	
Validation Max Data Length:	
Validation Strict Validation:	0
Validation Error Message:	

Max Repetitions:	1
Global:	0
Index:	Minimal
Auto Index:	1
Index Langauage:	English
Remote Type:	
Relative To:	
Relative To Path:	
Furigana Input Type:	
Furigana Field:	
Furigana Field ID:	
Furigana Field Basetable:	

Summary Operation:	
Summarize Repetition:	
Restart for Each Sorted Group:	
Summary Field:	
Summary Field ID:	
Summary Additional Field:	
Summary Additional Field ID:	
Summary Additional Field Table:	
Summary Additional Field Repetition:	

